try:
    a=int(input('Dame un número entero no negativo:'))
    b=int(input('Dame otro:'))
except:
    print("Los números deben ser enteros.")

if a <0 or b <0:
    print('Los números tienen que ser positivos.')
else:
    impares = []
    if a>b:
        aux=b
        b=a
        a=aux

    for num in range(a,(b+1)):
        if num%2 == 1:
            impares.append(num)
    final=sum(impares)
    print(final)

